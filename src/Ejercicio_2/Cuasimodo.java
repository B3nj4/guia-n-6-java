package Ejercicio_2;

import java.util.Random;
import java.util.HashSet;
import java.util.Iterator;

public class Cuasimodo {
	public static void main(String[] args) {
		//Creacion del conjunto de los participantes
		HashSet<String> Participantes = new HashSet<>();
		Participantes.add("A");
		Participantes.add("B");
		Participantes.add("C");
		Participantes.add("D");
		Participantes.add("E");
		Participantes.add("F");
		

		//Creacion del conjuntos de numeros para la victoria
		HashSet<Integer> Victoria = new HashSet<>();
		for(int i=1;i<7;i++) {
			Random numaleatorio = new Random();
			int r = numaleatorio.nextInt(6)+1;
			Victoria.add(r);
		}

		System.out.println("< Los numeros ganadores son >");	
		System.out.println(Victoria);
		
		
		//Creacion de los conjuntos de numeros para cada participante
		Iterator<String> iterate1 = Participantes.iterator();
		while(iterate1.hasNext()) {
			System.out.println("\nEl participante " + iterate1.next() + " completo los siguientes numeros: ");
			
			//Crea lista con los numeros de cada participante
			HashSet<Integer> Num_Parti = new HashSet<>();
			for(int i=1;i<7;i++) {
				Random numaleatorio = new Random();
				int rand = numaleatorio.nextInt(7);
				Num_Parti.add(rand);
			}
			System.out.println(Num_Parti);
			if(Victoria.containsAll(Num_Parti) == true){
				System.out.println("\nEl participante " + iterate1.next() + " a hecho bingo");
				break;
			}	
			
		}
		
	}
}
