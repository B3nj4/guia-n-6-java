package Ejercicio_1;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		
		System.out.println("< Creacion primer conjunto \"Estudiantes\" y su iteracion >");
		HashSet<String> Estudiantes = new HashSet<>();
		Estudiantes.add("A");
		Estudiantes.add("B");
		Estudiantes.add("C");
		Estudiantes.add("D");
		Estudiantes.add("E");
		Estudiantes.add("F");
		Iterator<String> iterate = Estudiantes.iterator();
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		
		
		System.out.println("\n< Busqueda de un nombre (predeterminado) dentro del conjunto \"Estudiantes\" >");
		Scanner sc = new Scanner(System.in);
		System.out.print("Introduzca nombre del alumno: ");
		String busca = sc.next();
		if (Estudiantes.contains(busca)) {
			System.out.println("Existe el nombre indicado");
		}
		else {
			System.out.println("No hay coincidencia");
		}
		
		
		System.out.println("\n< Creacion segundo conjunto \"estudiantes2020\" >");
		HashSet<String> estudiantes2020 = new HashSet<>();
		estudiantes2020.add("A");
		estudiantes2020.add("H");
		estudiantes2020.add("I");
		estudiantes2020.add("J");
		estudiantes2020.add("K");
		estudiantes2020.add("L");
		Iterator<String> iterate2 = estudiantes2020.iterator();
		while(iterate2.hasNext()) {
			System.out.println(iterate2.next());
		}
		
		
		System.out.println("\n< Consulta si los elementos de conjunto estudiante existen en \"estudiantes2020\" >");
		if (Estudiantes.containsAll(estudiantes2020) == true) {
			System.out.println("El conjunto \"estudiantes2020\" esta dentro de Estudiantes");
		}
		else if (Estudiantes.containsAll(estudiantes2020) == false) {
			System.out.println("El conjunto \"estudiantes2020\" no esta dentro de Estudiantes en su totalidad");
		}
		
		
		System.out.println("\n< Realizacion de una unión de los conjuntos de estudiantes >");
		Estudiantes.addAll(estudiantes2020);
		while(iterate.hasNext()) {
			System.out.println(iterate.next());
		}
		Iterator<String> iterate3 = Estudiantes.iterator();
		while(iterate3.hasNext()) {
			System.out.println(iterate3.next());
		}
		
		
		System.out.println("\n< Eliminacion de todos los estudiantes del conjunto \"estudiantes2020\" que no pertenezcan a \"Estudiantes\" >");
		Estudiantes.removeAll(estudiantes2020);
		Iterator<String> iterate4 = Estudiantes.iterator();
		while(iterate4.hasNext()) {
			System.out.println(iterate4.next());
		}
		
		
	
	}
}
